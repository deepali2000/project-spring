package newpackage.controller;

import newpackage.model.Agents;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;

@Controller
public class Agentcontroller {



    @RequestMapping("/agents")
    public String getAllAgentsP(Model model) throws JSONException {

        final String uri = "http://app.nearpe.com/businessentity/nearme?pincode=250001";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        final JSONObject obj = new JSONObject(result);
        final JSONArray agents = obj.getJSONArray("agents");
        final int n = agents.length();
        ArrayList<Agents> agentset=new ArrayList<>();
        for (int i = 0; i < n; ++i) {

            final JSONObject agent = agents.getJSONObject(i);

            Agents a=new Agents();
            a.setMobile(agent.getString("mobile"));
            a.setName(agent.getString("name"));
            a.setStoreName(agent.getString("storeName"));
            a.setGmapUrl(agent.getString("gmapUrl"));
            a.setPhotoUrl(agent.getString("photoUrl"));
            a.setAddress(agent.getString("address"));
            a.setPhotoUrl(agent.getString("photoUrl"));
            a.setHomePickup(agent.getBoolean("homePickup"));

            agentset.add(a);
        }
        model.addAttribute("agents",agentset);
        return "index";
    }

}
