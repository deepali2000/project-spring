package newpackage.model;

public class Agents {

    private String name;
    private String storeName;
    private String mobile;
    private String gmapUrl;
    private String photoUrl;
    private String address;
    private Double  lat;
    private Double lng;
    private boolean homePickup;



    public String getMobile() {
        return mobile;
    }

    public String getGmapUrl() {
        return gmapUrl;
    }

    public void setGmapUrl(String gmapUrl) {
        this.gmapUrl = gmapUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public boolean isHomePickup() {
        return homePickup;
    }

    public void setHomePickup(boolean homePickup) {
        this.homePickup = homePickup;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getMobile(String mobile) {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String toString()
    {
        return this.mobile+" "+this.name+" "+this.storeName+" "+this.gmapUrl+" "+this.photoUrl+" "+this.address+" "+this.photoUrl+" "+this.homePickup;


    }

}
