package newpackage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ServletComponentScan
@ComponentScan
public class DemoApplication {
    public static  void main(String ... x)
    {

        SpringApplication.run(DemoApplication.class,x);
    }

}